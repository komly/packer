package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"sort"
)

type Box struct {
	W int `json:"w"`
	H int `json:"h"`
}

type StateBox struct {
	*Box
	X int `json:"x"`
	Y int `json:"y"`
}

type Item struct {
	*Box
	X int
}

type Instance struct {
	Items []*Item
}

func (i *Instance) Clone() *Instance {
	items := make([]*Item, len(i.Items))
	for j := 0; j < len(i.Items); j++ {
		items[j] = &(*i.Items[j])
	}
	return &Instance{
		Items: items,
	}
}

func (i *Instance) Fitness() int {
	state := simulate(i.Items)
	minX := state[0].X
	minY := state[0].Y
	maxX := state[0].X
	maxY := state[0].Y
	for _, sbox := range state {
		if sbox.X < minX {
			minX = sbox.X
		}
		if sbox.X > maxX {
			maxX = sbox.X
		}
		if sbox.Y < minY {
			minY = sbox.Y
		}
		if sbox.Y > maxY {
			maxY = sbox.Y
		}
	}

	area := (maxX - minX) * (maxY - minY)
	return area + state[0].X

}

func generateBoxes(n int) []*Box {
	res := make([]*Box, 0)
	for i := 0; i < n; i++ {
		res = append(res, &Box{
			W: 50 + rand.Intn(50),
			H: 50 + rand.Intn(50),
		})
	}
	return res
}

func maxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func minInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func findY(state []*StateBox, item *Item) int {
	if len(state) == 0 {
		return 0
	}

	sort.SliceStable(state, func(i, j int) bool {
		return state[i].Y+state[i].Box.H > state[j].Y+state[i].Box.H
	})

	for _, box := range state {
		if maxInt(box.X+box.W, item.X+item.W)-minInt(box.X, item.X) < (box.W + item.W) {
			return box.Y + box.H
		}
	}

	return 0 //state[0].Y + state[0].Box.H
}

func findMinPossibleX(items []*Item) int {
	return 0
}

func simulate(items []*Item) []*StateBox {
	state := make([]*StateBox, 0, len(items))
	for _, item := range items {
		state = append(state, &StateBox{
			Box: item.Box,
			X:   item.X,
			Y:   findY(state, item),
		})
	}

	/*
		state[0].X = 0
		for i := 1; i < len(state); i++ {
			state[i].X = state[i-1].X + state[i-1].W
		}
	*/

	return state
}

func generateRandomPerm(boxes []*Box) []*Item {
	state := make([]*Item, 0, len(boxes))
	max_x := 0
	for _, box := range boxes {
		max_x += box.W
	}

	for _, box := range boxes {
		state = append(state, &Item{
			Box: box,
			X:   rand.Intn(max_x - box.W), //TODO: +1?
		})
	}

	return state
}

func main() {

	boxes := generateBoxes(10)
	inst := Instance{
		Items: generateRandomPerm(boxes),
	}
	bestInst := inst.Clone()
	bestFit := bestInst.Fitness()

	max_x := 0
	for _, box := range boxes {
		max_x += box.W
	}

	go func() {
		for {
			i, j := rand.Intn(len(inst.Items)), rand.Intn(len(inst.Items))
			lastX := inst.Items[i].X
			inst.Items[i], inst.Items[j] = inst.Items[j], inst.Items[i]
			inst.Items[i].X = rand.Intn(max_x)

			fit := inst.Fitness()
			if fit < bestFit {
				bestFit = fit
				bestInst = inst.Clone()
				log.Printf("Best fit: %d", bestFit)
			} else {
				inst.Items[i], inst.Items[j] = inst.Items[j], inst.Items[i]
				inst.Items[i].X = lastX
			}
		}
	}()

	http.HandleFunc("/state", func(w http.ResponseWriter, r *http.Request) {
		type StateResp struct {
			State []*StateBox `json:"state"`
		}
		state := simulate(bestInst.Items)
		resp := &StateResp{
			State: state,
		}
		if err := json.NewEncoder(w).Encode(resp); err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/index.html")
	})

	if err := http.ListenAndServe(":7000", nil); err != nil {
		log.Fatalf("can't listen http: %s", err)
	}
}
